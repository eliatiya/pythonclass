from random import randint
#כתבו תוכנית המקבלת 10 מספרים מהמשתמש ומדפיסה את הגדול ביותר.

listnum = []
num=0
for i in range(10):
    num = int(input("insert 10 number : "))
    listnum.append(num)
for j in listnum:
    if num < j :
        num = j
print(num)

#כתבו תוכנית המבקשת מהמשתמש להכניס את גילו ומדפיסה חזרה את הגיל בחודשים. אם המשתמש הכניס גיל שאינו מספר יש להציג הודעת שגיאה ולבקש שוב.

try:
    age = int(input("input your age in years : "))
    print(age * 12)
except ValueError:
    print("you enter worng value please type number only")
    age = int(input("input your age in years : "))
    print(age * 12)

#כתבו תוכנית הקוראת שורות מהמשתמש עד שהמשתמש מכניס שורה ריקה. לאחר הכנסת שורה ריקה יש להדפיס חזרה למשתמש את כל השורות שכתב מהסוף להתחלה.

row_list = []
while True:
    row = input("insert a sentense plaese > for finish click enter :")
    if row == '':
        print(row_list[::-1])
        break
    else:
        row_list.append(row)

#כתבו תוכנית המגרילה בלולאה מספרים שלמים בין 1 ל 1,000,000 עד שמוצאת מספר המתחלק גם ב-7, גם ב-13 וגם ב-15.

while True:
    num = randint(1,1000000)
    if num % 7 == 0 and num % 13 == 0 and num % 15 == 0:
        print(num)
        break
    else:
        continue
#כתבו תוכנית המגרילה שני מספרים בין 1 ל-10 ומחשבת את המכפלה המשותפת הקטנה ביותר שלהם, כלומר המספר הקטן ביותר שמתחלק בשני המספרים. אם לדוגמא הגרלתם את המספרים 4 ו-6 יש להדפיס חזרה את המספר 12.

num1 = randint(1,10)
num2 = randint(1,10)
for x in range(1,100000):
    if x % num1 == 0 and x % num2 == 0:
        print(x)
        break
    else:
        continuex=1

#כתבו תוכנית הבוחרת באקראי מספר בין 1 ל-100. על המשתמש לנחש את המספר שנבחר ואחרי כל ניחוש יש להדפיס ״גדול מדי״ או ״קטן מדי״ לפי היחס בין המספר שנבחר לניחוש. בונוס: כדי שיהיה מעניין דאגו שמדי פעם התוכנית תדפיס את ההודעה הלא נכונה.

rand = randint(1,1000)
tries = 0
while True:
        user_guess = int(input("What is the number ? > "))
        if user_guess == rand:
                break

        elif user_guess > rand:
                print("Your number is bigger")
                tries += 1
        else:
                print("Your number is lower")
                tries += 1

print ("You win in {0} tries".format(tries))